﻿using System;
using System.Collections.Generic;

namespace TowersOfHanoiTask
{
    /// <summary>
    /// Represents Hanoi Towers puzzle.
    /// </summary>
    public static class HanoiTowerPuzzle
    {
        /// <summary>
        /// Moves all disks from sourceTower to destinationTower using recursive algorithm and returns right sequence.
        /// </summary>
        /// <param name="count">Count of disks.</param>
        /// <param name="sourceTower">Source tower number.</param>
        /// <param name="destinationTower">Destination tower number.</param>
        /// <returns>Sequence of moves.</returns>
        public static IEnumerable<(int, int)> MoveDisks(int count, int sourceTower, int destinationTower)
        {
            if (count <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "Count can't be less or equal to 0.");
            }

            if (sourceTower > 3 || sourceTower < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(sourceTower),
                    "Source Tower number can't be less than 1 and more than 3.");
            }

            if (destinationTower > 3 || destinationTower < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(destinationTower),
                    "Destination Tower number can't be less than 1 and more than 3.");
            }

            if (sourceTower == destinationTower)
            {
                throw new ArgumentException("Source and destination towers can't be the same.");
            }

            return MoveDisksRecursive(count, sourceTower, destinationTower);
        }

        private static IEnumerable<(int, int)> MoveDisksRecursive(int count, int sourceTower, int destinationTower)
        {
            if (count-- <= 0) yield break;
            
            int tmp = 6 - sourceTower - destinationTower;
                
            foreach (var t in MoveDisksRecursive(count, sourceTower, tmp))
            {
                yield return t;
            }

            yield return (sourceTower, destinationTower);

            foreach (var t in MoveDisksRecursive(count, tmp, destinationTower))
            {
                yield return t;
            }
        }
    }
}