﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;

namespace TowersOfHanoiTask.Tests
{
    public static class HanoiTowersTestCases
    {
        public static IEnumerable TestCases
        {
            get
            {
                yield return new TestCaseData((sbyte)2, (sbyte)1, (sbyte)2, new List<(int, int)>
                {
                    new (1, 3),
                    new (1, 2),
                    new (3, 2),
                });
                yield return new TestCaseData((sbyte)2, (sbyte)1, (sbyte)3, new List<(int, int)>
                {
                    new (1, 2),
                    new (1, 3),
                    new (2, 3),
                });
                yield return new TestCaseData((sbyte)3, (sbyte)1, (sbyte)2, new List<(int, int)>
                {
                    new (1, 2),
                    new (1, 3),
                    new (2, 3),
                    new (1, 2),
                    new (3, 1),
                    new (3, 2),
                    new (1, 2),
                });
            }
        }
    }
}