using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace TowersOfHanoiTask.Tests
{
    [TestFixture]
    public class HanoiTowerPuzzleTests
    {
        [Test]
        [TestCaseSource(typeof(HanoiTowersTestCases), "TestCases")]
        public void MoveDisks_ParametersAreValid(int count, int sourceTower, int destinationTower, List<(int, int)> expected)
        {
            Assert.AreEqual(expected,HanoiTowerPuzzle.MoveDisks(count, sourceTower, destinationTower));
        }

        [TestCase(-1, 1, 3)]
        [TestCase(0, -1, 3)]
        [TestCase(5, 0, 4)]
        public void MoveDisks_ParametersAreInvalid(int count, int sourceTower, int destinationTower)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => HanoiTowerPuzzle.MoveDisks(count, sourceTower, destinationTower));
        }

        [TestCase(3, 1, 1)]
        public void MoveDisks_SourceAndDestinationAreTheSame(int count, int sourceTower, int destinationTower)
        {
            Assert.Throws<ArgumentException>(() => HanoiTowerPuzzle.MoveDisks(count, sourceTower, destinationTower));
        }
    }
}